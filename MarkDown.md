# Язык разметки MarkDown

В GitLab (и на многих других ресурсах) для удобства редактирования можно использовать разметку MarkDown. Подробное описание есть здесь: https://gitlab.com/help/user/markdown.

## Базовые правила

Разметка | Эффект
--- | ---
`# Заголовок 1` | <span style="font-weight: bold; font-size: 2em">Заголовок 1</span>
`## Заголовок 2` | <span style="font-weight: bold; font-size: 1.5em">Заголовок 1</span>
`### Заголовок 3` | <span style="font-weight: bold; font-size: 1em">Заголовок 1</span>
<code>Строки<br>без<br>переноса</code> | Строки без переноса
<code>Два пробела··<br>в конце строки —··<br>новая строка</code> | Два пробела<br>в конце строки —<br>новая строка
<code>Между абзацами<br><br>Пустая строка</code> | Между абзацами<br><br>Пустая строка
`*Наклонный*`, `_Наклонный_` | *Наклонный*
`**Жирный**`, `__Жирный__` | **Жирный**
`~~Зачеркнутый~~` | ~~Зачеркнутый~~
<code>&#0096;Программный код&#0096;</code> | `Программный код`
<code>&#0096;&#0096;&#0096;<br>Программный код<br>&nbsp;&nbsp;на много<br>строк<br>&#0096;&#0096;&#0096;</code> | <code>Программный код<br>&nbsp;&nbsp;на много<br>строк</code>
<code>&#0096;&#0096;&#0096;bsl<br>// Пример кода 1С<br>Функция Код1С()<br>&nbsp;&nbsp;&nbsp;&nbsp;Возврат "Текст ошибки";<br>КонецФункции<br>&#0096;&#0096;&#0096;</code> | <div style="font-family: monospace"><span style="color: #998; font-style: italic">// Пример кода 1С</span><br><span style="font-weight:600">Функция</span> <span style="color:#008080">Код1С</span>()<br>&nbsp;&nbsp;&nbsp;&nbsp;<span style="font-weight:600">Возврат</span> <span style="color:#d14">"Текст ошибки"</span>;<br><span style="font-weight:600">КонецФункции</span></div>
<code>Список:<br>&nbsp;- раз<br>&nbsp;- два<br>&nbsp;- три</code> | Список:<br>&nbsp;&nbsp;• раз<br>&nbsp;&nbsp;• два<br>&nbsp;&nbsp;• три
<code>Нумерованный список:<br>1. раз<br>1. два<br>2. три<br>10. четыре</code> | Нумерованный список:<br>&nbsp;&nbsp;1. раз<br>&nbsp;&nbsp;2. два<br>&nbsp;&nbsp;3. три<br>&nbsp;&nbsp;4. четыре
`---` | Горизонтальная черта
`![Название](FileName "Подсказка")` | Картинка, видео, аудио

## Таблицы

И можно сделать таблицу:  
<code>Колонка&nbsp;&nbsp;&nbsp; | Выравнивание слева | Выравнивание справа | Выравнивание по центру | <br>---------- | :----------------- | ---: | :--:<br>Ячейка 1&nbsp;&nbsp; | Ячейка 2&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; | Ячейка 3 | Ячейка 4<br>Ячейка 1.1 | Ячейка 1.2&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; | Ячейка 1.3 | Ячейка 1.4</code>

Колонка    | Выравнивание слева | Выравнивание справа | Выравнивание по центру
---------- | :----------------- | ---: | :--:
Ячейка 1   | Ячейка 2           | Ячейка 3 | Ячейка 4
Ячейка 1.1 | Ячейка 1.2         | Ячейка 1.3 | Ячейка 1.4

## Диаграммы

Можно делать диаграммы в формате mermaid (это должен поддерживать сервер gitlab'а):  
<code>&#0096;&#0096;&#0096;mermaid
graph TD
&nbsp;&nbsp;&nbsp;&nbsp;CFStore[Хранилище конфигурации]
&nbsp;&nbsp;&nbsp;&nbsp;DevBase1[(База разработки 1)] --> CFStore
&nbsp;&nbsp;&nbsp;&nbsp;DevBase2[(База разработки 2)] --> CFStore
&nbsp;&nbsp;&nbsp;&nbsp;DevBase3[(База разработки 3)] --> CFStore
&nbsp;&nbsp;&nbsp;&nbsp;CFStore -. Программный код .-> gitlab(GitLab)
&nbsp;&nbsp;&nbsp;&nbsp;
&nbsp;&nbsp;&nbsp;&nbsp;Jenkins(Jenkins) -- Управляет --> JenkinsAgent1(Агент Jenkins)
&nbsp;&nbsp;&nbsp;&nbsp;JenkinsAgent1 -- Обновление --> TestBase[(База для тестирования)]
&nbsp;&nbsp;&nbsp;&nbsp;JenkinsAgent1 -- Тестирование --> TestBase
&nbsp;&nbsp;&nbsp;&nbsp;
&nbsp;&nbsp;&nbsp;&nbsp;Test.dt -. Модельная база .-> TestBase
&nbsp;&nbsp;&nbsp;&nbsp;gitlab -. Актуальная конфигурация .-> TestBase
&#0096;&#0096;&#0096;</code>

```mermaid
graph TD
    CFStore[Хранилище конфигурации]
    DevBase1[(База разработки 1)] --> CFStore
    DevBase2[(База разработки 2)] --> CFStore
    DevBase3[(База разработки 3)] --> CFStore
    CFStore -. Программный код .-> gitlab(GitLab)

    Jenkins(Jenkins) -- Управляет --> JenkinsAgent1(Агент Jenkins)
    JenkinsAgent1 -- Обновление --> TestBase[(База для тестирования)]
    JenkinsAgent1 -- Тестирование --> TestBase

    Test.dt -. Модельная база .-> TestBase
    gitlab -. Актуальная конфигурация .-> TestBase
```

Ещё примеры в [Разработка](Разработка.md). Описание: https://mermaid-js.github.io/mermaid/